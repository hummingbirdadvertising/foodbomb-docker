CREATE TABLE Employees
(
    ID int NOT NULL AUTO_INCREMENT,
    EmpName varchar(50) NOT NULL,
    Wage varchar(50) NOT NULL,
    PRIMARY KEY (ID)
);

INSERT INTO Employees
    (EmpName, Wage)
VALUES
    ('Tom', 10.5);

INSERT INTO Employees
    (EmpName, Wage)
VALUES
    ('Jane', 11);

INSERT INTO Employees
    (EmpName, Wage)
VALUES
    ('Mike', 50);
    
INSERT INTO Employees
    (EmpName, Wage)
VALUES
    ('John', 150);