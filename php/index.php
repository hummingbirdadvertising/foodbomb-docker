<?php

$servername = $_ENV['DB_HOST'];
$dbname = "my-database";
$username = "root";
$password = "my-secret-password";

try {
    $db = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    // set the PDO error mode to exception
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    echo "Connected successfully" . "</br></br>"; 
    }
catch(PDOException $e)
    {
    echo "Connection failed: " . $e->getMessage();
    }

$sql = "SELECT * FROM Employees;";
$stmt = $db->query($sql); 
$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);


foreach ($rows as $employee) {
    echo "<pre>";
    foreach ($employee as $details) {
        echo "<pre>";
        print_r($details);
        echo "</pre>";
    }
    echo "</pre>";
}

?>